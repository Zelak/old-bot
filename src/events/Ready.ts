import { Redo } from "../classes/Redo";

export default class Ready {
	public name: string;

	constructor() {
		this.name = "ready";
	}

	exec(client: Redo) {
		console.log("Ready!");
	}
}