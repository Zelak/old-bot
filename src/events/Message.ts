import { Message } from "discord.js";
import { Redo } from "../classes/Redo";

export default class MessageE {
	public name: string;

	constructor() {
		this.name = "message";
	}

	async exec(client: Redo, msg: Message) {
		const input = msg.content.toLowerCase();
		if (msg.guild && msg.guild.id === "527586003913801738") {
			console.log(msg.member!!.permLevel(client));
		}
		if (!msg.content.startsWith(client.config.prefix)) return this.otherProcess(client, msg);
		if (msg.author.bot) return;

		const cmd = input.split(" ")[0].replace(client.config.prefix, "");
		const args = msg.content.split(" ").slice(1);

		// process command
		const command = client.commandHandler.get(cmd);
		if (command) {
			const required = client.commandHandler.verifyRequires(client, msg, command);
			if (required.length != 0) {
				msg.channel.send(`Required for this command:\n\`\`\`${required.join("\n")}\`\`\``);
				return;
			}

			try {
				await command.exec(msg, args);
			} catch (e) {
				msg.channel.send(e);
			}
		}
	}

	otherProcess(client: Redo, msg: Message) {
		// Process backups
		client.backupsHandler.process(msg);
	}
}