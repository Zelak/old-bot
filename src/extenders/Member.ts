import { Structures } from "discord.js";
import { Redo } from "../classes/Redo";

declare module "discord.js" {
	interface GuildMember {
		permLevel(client: Redo): number;
	}
}

export default Structures.extend("GuildMember", GuildMember => class extends GuildMember {
	tempPerm: number = -1;

	permLevel(client: Redo): number {
		if (this.tempPerm != -1) return this.tempPerm;

		let temp = 0;
		for (const i in client.config.permissions) {
			if (i == "owner") {
				if (client.config.owners.includes(this.id)) temp |= client.bitField.get(i);
			} else {
				if (this.roles.has(client.config.permissions[i])) temp |= client.bitField.get(i);
			}
		}

		this.tempPerm = temp;
		return temp;
	}

});