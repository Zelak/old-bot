import { Redo } from "./classes/Redo";
import ExtenderHandler from "./classes/Handlers/ExtenderHandler";
const extenderHandler = new ExtenderHandler("../../extenders");
extenderHandler.init().then(() => {
	new Redo();
});