import { Entity, ObjectID, ObjectIdColumn, Column } from "typeorm";

@Entity()
export class User {

	constructor(name: string, last: string) {
		this.firstName = name;
		this.lastName = last;
	}

	@ObjectIdColumn()
    id!: ObjectID;

	@Column()
	firstName!: string;

	@Column()
	lastName!: string;

}