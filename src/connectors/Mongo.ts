import { createConnection, Connection, getMongoManager, MongoEntityManager } from "typeorm";
import { User } from "./entities/User"

export default class Mongo {
	public connection: Connection | undefined;
	public defer: Promise<any>;
	public manager: MongoEntityManager | undefined;
	public connected: Boolean = false;

	constructor() {
		this.defer = new Promise(async (res, rej) => {
			try {
				await this.login();
				this.connected = true;
				res();
			} catch (e) {
				rej(e);
			}
		});
	}

	async login() {
		this.connection = await createConnection({
			type: "mongodb",
			host: "localhost",
			port: 27017,
			database: "test",
			entities: [
				User
			]
		});

		this.manager = getMongoManager();
	}

	async save<Entity>(ent: Entity): Promise<Entity> {
		return this.manager!!.save(ent);
	}

}

new Mongo();