import { Message } from "discord.js";
import { ICommand } from "../classes/Handlers/CommandHandler";

export default class Ping implements ICommand {
	public name: string;
	public aliases: string[];
	public required: ICommand["required"];

	constructor() {
		this.name = "ping";
		this.aliases = ["p"];
		this.required = {
			owner: false,
			dbConnection: false,
			guild: false,
			private: false,
			inVoiceChannel: false
		}
	}

	async exec(msg: Message) {
		const newM = await msg.channel.send("yoof!");
		await newM.edit(`Pong! | ${newM.createdTimestamp - msg.createdTimestamp} ms`);
	}

}