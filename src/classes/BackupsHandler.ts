import * as nextra from "fs-nextra";
import { Message } from "discord.js";
import { resolve } from "path";
import request = require("request");

interface BackupsOptions {
	type: string // Type of backup
	id: string // The id of the channel
	folder: string // The folder to back it up
}

export default class BackupsHandler {
	private options: BackupsOptions[];

	constructor(options: BackupsOptions[]) {
		this.options = options;
	}

	async process(msg: Message) {
		for (const option of this.options) {
			if (option.id == msg.channel.id) {
				const path = resolve(__dirname, option.folder);

				if (option.type === "img") {
					// Backups imgs
					if (msg.attachments.size == 0) continue;
					for (const att of msg.attachments.values()) {
						if (att.name == null) continue;

						const extension = att.name.split(".").pop();
						const fileName = `${att.name.split(".").slice(0, -1)}(${att.id}).${extension}`;
						const reOption = {
							url: att.url,
							encoding: null
						}
						const img = request.get(reOption);
						img.pipe(nextra.createWriteStream(`${path}/${fileName}`));
					}
				}

				// break; No break if there is more backups from same chan
			}
		}
	}
}