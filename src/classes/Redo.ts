import { Client } from "discord.js";
import EventHandler from "./Handlers/EventHandler";
import CommandHandler from "./Handlers/CommandHandler";
import BackupsHandler from "./BackupsHandler";
import Mongo from "../connectors/Mongo";
import { BitField } from "bitfields";
import ExtenderHandler from "./Handlers/ExtenderHandler";
const config = require("../../config.json") as any;

// Main bot class extending the Client
export class Redo extends Client {
	public eventHandler: EventHandler;
	public commandHandler: CommandHandler;
	public extenderHandler: ExtenderHandler;
	public backupsHandler: BackupsHandler;
	public bitField: BitField;
	public mongo: Mongo;
	public config: any;

	constructor() {
		super();

		this.config = config;
		this.eventHandler = new EventHandler(this, config.handlers.events);
		this.commandHandler = new CommandHandler(config.handlers.commands);
		this.extenderHandler = new ExtenderHandler(config.handlers.extenders);
		this.backupsHandler = new BackupsHandler(config.backups);
		this.bitField = new BitField();
		this.mongo = new Mongo();

		this.init();
	}

	async init() {

		for (const i in this.config.permissions) {
			this.bitField.set(i);
		}

		await Promise.all([
			this.eventHandler.init(),
			this.commandHandler.init(),
			this.extenderHandler.init(),
			this.mongo.defer
		]);

		this.login(config.token);
	}

}