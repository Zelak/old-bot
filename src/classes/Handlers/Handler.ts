import * as nextra from "fs-nextra";
import { resolve, normalize } from "path";

// Handles files (commands/events)
const error = (msg: string) => { throw new Error(msg) };

export default class Handler<K, V> extends Map<K, V> {
	public dir: string;
	public options: any;

	constructor(dir: string = error("No dir set"), options: any = {}) {
		super();
		this.dir = resolve(__dirname, dir);
		this.options = options;
	}

	async init() {
		if (!await nextra.pathExists(this.dir)) throw new Error("Dir path doesn't exist");
		this.processFolder(this.dir);
	}

	async processFolder(dir: string) {

		const files = await nextra.readdir(dir);
		for (const file of files) {
			if (!file.endsWith(".js")) continue;

			const _path = normalize(`${dir}\\${file}`);
			const lstats = await nextra.lstat(_path);
			if (!lstats.isFile()) {
				this.processFolder(_path);
				continue
			}

			this.accessFile(_path);
		}
	}

	accessFile(path: string) { // Access the file with a require and add it to the map
		console.log(path);
		try {
			const _required = require(path).default;
			delete require.cache[require.resolve(path)];
			if (!this.options.ex) {
				const constructed = new _required();
				this.set(constructed.name, constructed);
			}
		} catch (e) {
			console.log(e);
		}
	}

}