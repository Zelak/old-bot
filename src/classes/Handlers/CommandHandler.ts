import Handler from "./Handler";
import * as nextra from "fs-nextra";
import { Message } from "discord.js";
import { Redo } from "../Redo";

export interface ICommand {
	name: string;
	aliases: string[] | undefined;
	required: {
		owner: Boolean;
		dbConnection: Boolean;
		guild: Boolean;
		private: Boolean;
		inVoiceChannel: Boolean;
	}
	exec: (msg: Message, args: string[]) => {};
}

export default class CommandHandler extends Handler<string, ICommand> {
	private aliases: Map<string, string>;

	constructor(dir: string, options = {}) {
		super(dir, options);
		this.aliases = new Map();

		// Watch files for realods
		const watcher = nextra.watch(this.dir);
		let fileCounter = 1;
		let lastFile = "";
		let justChanged = false;
		watcher.on("change", (_, filename: string) => {
			if (lastFile == filename) {
				fileCounter++;
				justChanged = true;
			} else lastFile = filename;

			if ((fileCounter == 3 && justChanged) || (fileCounter == 4 && !justChanged)) {
				fileCounter = 1;
				justChanged = false;
				this.accessFile(this.dir + "\\" + filename);
			}
		});
	}

	set(key: string, value: ICommand) {
		if (value.aliases) {
			for (const i of value.aliases) {
				this.aliases.set(i, value.name);
			}
		}

		return super.set(key, value);
	}

	get(key: string) {
		return super.get(key) || super.get(this.aliases.get(key)!);
	}

	verifyRequires(client: Redo, msg: Message, cmd: ICommand): string[] {
		const required = [];
		if (cmd.required.dbConnection && !client.mongo.connected)
			required.push("Database connection");
		if (cmd.required.owner && !client.config.owners.includes(msg.author.id))
			required.push("Ownership of the bot");
		if (cmd.required.guild && !msg.guild)
			required.push("Needs to be in a guild");
		if (cmd.required.private && !(msg.channel.type == "dm"))
			required.push("Needs to be in a DM");
		if (cmd.required.inVoiceChannel && !msg.member!!.voice.channel)
			required.push("Need to be in a voice channel");

		return required;
	}

}