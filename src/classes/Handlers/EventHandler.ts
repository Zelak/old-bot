import Handler from "./Handler";
import { Redo } from "../Redo";

export interface IEvent {
	name: string;
	exec: () => {};
	bound: (...[]) => {} | undefined;
}

export default class EventHandler extends Handler<string, IEvent> {
	public client: Redo;

	constructor(client: Redo, dir: string, options = {}) {
		super(dir, options);
		this.client = client;
	}

	set(key: string, value: IEvent) { // overrwrite the set function from map
		const bound = value.exec.bind(value, this.client);
		this.client.on(key, bound);
		value.bound = bound;

		return super.set(key, value);
	}

}