import Handler from "./Handler";

export default class ExtenderHandler extends Handler<string, () => {}> {

	constructor(dir: string, options: any = {}) {
		options.ex = true;
		super(dir, options);
	}

}